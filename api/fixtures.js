const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require("./models/User");
const Photo = require("./models/Photo");

const run = async () => {
    await mongoose.connect(config.db.url);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [user1, user2, user3, user4] = await User.create({
        email: 'admin@admin.com',
        password: 'admin',
        token: nanoid(),
        displayName: "Admin"
    }, {
        email: 'root@root.com',
        password: 'root',
        token: nanoid(),
        displayName: "Root"
    }, {
        email: 'user@user.com',
        password: 'user',
        token: nanoid(),
        displayName: "User"
    }, {
        email: 'azat@azat.com',
        password: 'azat',
        token: nanoid(),
        displayName: "Azat"
    });

    await Photo.create({
        title: "Beautiful picture!",
        author: user1,
        image: "fixtures/img-1.jpg",
    }, {
        title: "Cool nature",
        author: user1,
        image: "fixtures/img-2.jpg",
    }, {
        title: "She didn't notice me",
        author: user1,
        image: "fixtures/img-3.png",
    }, {
        title: "Mountains are beautiful",
        author: user2,
        image: "fixtures/img-4.jpg",
    }, {
        title: "Tow dogs looking at each other",
        author: user2,
        image: "fixtures/img-5.jpg",
    }, {
        title: "Legendary",
        author: user3,
        image: "fixtures/img-6.jpg",
    }, {
        title: "Always wanted to try",
        author: user3,
        image: "fixtures/img-7.jpg",
    }, {
        title: "Bast nerd ever lived",
        author: user4,
        image: "fixtures/img-8.jpg",
    }, {
        title: "Crush",
        author: user4,
        image: "fixtures/img-9.jpg",
    });

    await mongoose.connection.close();
};

run().catch(console.error);