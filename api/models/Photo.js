const mongoose = require('mongoose');
const Shema = mongoose.Schema;

const PhotoShema = new Shema({
    title: {
        type: String,
        required: true,
    },
    image: {
        type: String,
        required: true
    },
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    createdAt: {
        type: Date,
        default: () => Date.now() + 7*24*60*60*1000,
    }
});

const Photo = mongoose.model('Photo', PhotoShema);
module.exports = Photo;