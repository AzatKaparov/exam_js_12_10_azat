const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Photo = require("../models/Photo");
const auth = require("../middleware/auth");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const photos = await Photo.find().populate("author", "displayName").sort({createdAt: -1});

        return res.status(200).send(photos);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const photo =  await Photo.find({author: req.params.id}).populate("author", "displayName").sort({createdAt: -1});

        if (photo) {
            return res.status(200).send(photo);
        } else {
            return res.status(404).send({error: 'Photos not found'});
        }
    } catch (e) {
        res.send(e);
    }
});

router.post('/', [auth, upload.single('image')], async (req, res) => {
    const photoData = {
        title: req.body.title,
        author: req.user,
    };

    if (req.file) {
        photoData.image = `uploads/${req.file.filename}`;
    }

    const photo = new Photo(photoData);

    try {
        await photo.save();
        res.status(200).send(photo);
    } catch (error) {
        return res.status(400).send(error);
    }
});

router.delete("/:id", auth, async (req, res) => {
    try {
        const photo = await Photo.findOne({_id: req.params.id});

        if (photo.author.equals(req.user._id)) {
            photo.delete();
        }

        return res.send(photo);
    } catch (e) {
        res.status(500).send(e);
    }
});


module.exports = router;