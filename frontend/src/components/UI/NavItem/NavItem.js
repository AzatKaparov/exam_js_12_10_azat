import React from 'react';
import {NavLink} from "react-router-dom";
import {Nav} from "react-bootstrap";

const NavItem = ({to, title}) => {
    return (
        <Nav.Item>
            <NavLink to={to} className="mr-3 text-white">{title}</NavLink>
        </Nav.Item>
    );
};

export default NavItem;