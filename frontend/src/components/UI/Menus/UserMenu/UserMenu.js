import React from 'react';
import {Button} from "react-bootstrap";
import {NavLink} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {logoutUser} from "../../../../store/actions/usersActions";
import "./UserMenu.css"
import NavItem from "../../NavItem/NavItem";

const UserMenu = () => {
    const dispatch = useDispatch();
    const { user } = useSelector(state => state.users);
    const logout = () => {dispatch(logoutUser())};

    return (
        <>
            <NavItem to="/add-photo" title="Add photo"/>
            <div className="d-flex align-items-center ml-auto">
                <NavLink to={`/gallery/${user._id}`} className="nav-user-link my-0 mx-2 ">{user?.displayName}</NavLink>
                <Button onClick={logout} variant="warning">Logout</Button>
            </div>
        </>
    );
};

export default UserMenu;