import React from 'react';
import PropTypes from 'prop-types';
import {Button, Card, Col} from "react-bootstrap";
import {API_URL} from "../../constants";
import './PhotoPreview.css';
import {NavLink} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {deletePhoto} from "../../store/actions/photosActions";

const PhotoPreview = ({ img, author, title, showModal, id }) => {
    const dispatch = useDispatch();
    const { user } = useSelector(state => state.users);

    return (
        <Col xl={4} className="mb-4">
            <Card>
                <Card.Img onClick={() => showModal(`${API_URL}/${img}`)} className="PhotoPreviewImage" variant="top" src={`${API_URL}/${img}`} />
                <Card.Body>
                    <Card.Title>
                        <NavLink to={`/gallery/${author._id}`}>
                            {author.displayName}
                        </NavLink>
                    </Card.Title>
                    <Card.Text>{title}</Card.Text>
                </Card.Body>
                {
                    user?._id === author._id &&
                    <Card.Footer>
                        <Button onClick={() => dispatch(deletePhoto(id))} variant="danger">Delete</Button>
                    </Card.Footer>
                }
            </Card>
        </Col>
    );
};

PhotoPreview.propTypes = {
    img: PropTypes.string.isRequired,
    author: PropTypes.object.isRequired,
    title: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
}

export default PhotoPreview;