export const API_URL = "http://localhost:8000";

export const getFieldError = (error, fieldName) => {
    try {
        return error.errors[fieldName].message;
    } catch (e) {
        return undefined;
    }
};