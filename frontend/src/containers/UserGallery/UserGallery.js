import React, {useEffect, useState} from 'react';
import Preloader from "../../components/UI/Preloader/Preloader";
import Backdrop from "../../components/UI/Backdrop/Backdrop";
import {Row} from "react-bootstrap";
import PhotoPreview from "../../components/PhotoPreview/PhotoPreview";
import Popup from "../../components/UI/Popup/Popup";
import {useDispatch, useSelector} from "react-redux";
import {fetchPhotos} from "../../store/actions/photosActions";

const UserGallery = ({ match }) => {
    const dispatch = useDispatch();
    const { photos, photosLoading } = useSelector(state => state.photos);
    const [modal, setModal] = useState({
        img: "",
        show: null,
    });

    const showModalHandler = (img) => {
        setModal({
            img: img,
            show: true
        })
    };

    const hideModalHandler = () => {
        setModal({
            ...modal,
            show: false
        });
    };

    useEffect(() => {
        if (match.params.id) {
            dispatch(fetchPhotos(match.params.id));
        }
    }, [dispatch, match.params.id]);

    return (
        <>
            <Popup show={modal.show} img={modal.img} hide={hideModalHandler} />
            <Backdrop show={modal.show} onClick={() => hideModalHandler()} />
            <Preloader show={photosLoading} />
            <Backdrop show={photosLoading} />
            <div className="container">
                {
                    photos?.length > 0 &&
                    <h1>{photos[0].author.displayName}'s gallery</h1>
                }
                <Row className="justify-content-center">
                    {photos.map(photo => (
                            <PhotoPreview
                                id={photo._id}
                                showModal={showModalHandler}
                                key={photo._id}
                                author={photo.author}
                                img={photo.image}
                                title={photo.title}
                            />
                        ))}
                </Row>
            </div>
        </>
    );
};

export default UserGallery;