import React, {useState} from 'react';
import {Button, Form} from "react-bootstrap";
import FormElement from "../../components/UI/FormElement/FormElement";
import {useDispatch, useSelector} from "react-redux";
import {createPhoto} from "../../store/actions/photosActions";
import Preloader from "../../components/UI/Preloader/Preloader";
import Backdrop from "../../components/UI/Backdrop/Backdrop";
import {getFieldError} from "../../constants";

const AddPhoto = () => {
    const dispatch = useDispatch();
    const { photosError, photosLoading } = useSelector(state => state.photos);
    const [options, setOptions] = useState({
        title: "",
        image: "",
    });

    const inputChangeHandler = e => {
        const {name, value} = e.target;
        setOptions(prevState => ({...prevState, [name]: value}));
    };

    const onFileChange = e => {
        const name = e.target.name;
        const file = e.target.files[0];

        setOptions(prevState => ({
            ...prevState,
            [name]: file
        }));
    };

    const onFormSubmit = async photoData => {
        await dispatch(createPhoto(photoData));
    };

    const submitFormHandler  = e => {
        e.preventDefault();
        const formData = new FormData();

        Object.keys(options).forEach(key => {
            formData.append(key, options[key]);
        });
        onFormSubmit(formData).catch(err => console.error(err));
    };

    return (
        <>
            <Preloader show={photosLoading} />
            <Backdrop show={photosLoading} />
            <div className="container">
                <Form onSubmit={submitFormHandler}>
                    <FormElement
                        label="Title"
                        name="title"
                        value={options.title}
                        onChangeHandler={inputChangeHandler}
                        type="text"
                        placeholder="Title"
                        error={getFieldError(photosError, "title")}
                        required
                    />
                    <FormElement
                        label="Image"
                        name="image"
                        onChangeHandler={onFileChange}
                        type="file"
                        error={getFieldError(photosError, "image")}
                        required
                        file
                    />
                    <Button className="my-3 btn-lg" type="submit" variant="success">Create</Button>
                </Form>
            </div>
        </>
    );
};

export default AddPhoto;