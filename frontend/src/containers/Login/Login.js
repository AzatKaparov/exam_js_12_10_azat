import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Button, Form} from "react-bootstrap";
import FormElement from "../../components/UI/FormElement/FormElement";
import {loginUser} from "../../store/actions/usersActions";
import {NavLink} from "react-router-dom";
import {getFieldError} from "../../constants";
import FacebookLogin from "../../components/UI/FacebookLogin/FacebookLogin";

const Login = () => {
    const dispatch = useDispatch();
    const {loginError} = useSelector(state => state.users);
    const [user, setUser] = useState({
        email: "",
        password: "",
    });


    const inputChangeHandler = e => {
        const {name, value} = e.target;

        setUser(prevState => ({...prevState, [name]: value}));
    };

    const submitFormHandler = e => {
        e.preventDefault();
        dispatch(loginUser({...user}));
    };

    return (
        <div className="container">
            <Form onSubmit={submitFormHandler}>
                <FormElement
                    value={user.email}
                    name="email"
                    onChangeHandler={inputChangeHandler}
                    type="email"
                    placeholder="Email"
                    label="Email"
                    error={getFieldError(loginError, "email")}
                    required
                />
                <FormElement
                    value={user.password}
                    name="password"
                    onChangeHandler={inputChangeHandler}
                    type="password"
                    placeholder="Password"
                    label="Password"
                    error={getFieldError(loginError, "password")}
                    required
                />

                <Form.Text className="my-3">
                    Don't have an account?
                    <NavLink className="mx-2" to="/register">Register now</NavLink>
                </Form.Text>
                <FacebookLogin />
                <br/>
                <Button className="my-3" type="submit" variant="primary">
                    Submit
                </Button>
            </Form>
        </div>
    );
};

export default Login;