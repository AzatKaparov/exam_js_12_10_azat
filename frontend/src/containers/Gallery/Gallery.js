import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchPhotos} from "../../store/actions/photosActions";
import PhotoPreview from "../../components/PhotoPreview/PhotoPreview";
import {Row} from "react-bootstrap";
import Backdrop from "../../components/UI/Backdrop/Backdrop";
import Preloader from "../../components/UI/Preloader/Preloader";
import Popup from "../../components/UI/Popup/Popup";

const Gallery = () => {
    const dispatch = useDispatch();
    const { photos, photosLoading } = useSelector(state => state.photos);
    const [modal, setModal] = useState({
        img: "",
        show: null,
    });

    const showModalHandler = (img) => {
        setModal({
            img: img,
            show: true
        })
    };

    const hideModalHandler = () => {
        setModal({
            ...modal,
            show: false
        });
    };


    useEffect(() => {
        dispatch(fetchPhotos());
    }, [dispatch])

    return (
        <>
            <Popup show={modal.show} img={modal.img} hide={hideModalHandler} />
            <Backdrop show={modal.show} onClick={() => hideModalHandler()} />
            <Preloader show={photosLoading} />
            <Backdrop show={photosLoading} />
            <div className="container">
                <Row className="justify-content-center">
                    {photos?.length > 0
                        ? photos.map(photo => (
                        <PhotoPreview
                            id={photo._id}
                            showModal={showModalHandler}
                            key={photo._id}
                            author={photo.author}
                            img={photo.image}
                            title={photo.title}
                        />
                    ))
                        : <h1>There is no photos</h1>
                    }
                </Row>
            </div>
        </>
    );
};

export default Gallery;