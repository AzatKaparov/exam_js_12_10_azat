import {axiosApi} from "../../axiosApi";
import {historyPush} from "./historyActions";

export const FETCH_PHOTOS_REQUEST = "FETCH_PHOTOS_REQUEST";
export const FETCH_PHOTOS_SUCCESS = "FETCH_PHOTOS_SUCCESS";
export const FETCH_PHOTOS_FAILURE = "FETCH_PHOTOS_FAILURE";

export const fetchPhotosRequest = () => ({type: FETCH_PHOTOS_REQUEST});
export const fetchPhotosSuccess = photos => ({type: FETCH_PHOTOS_SUCCESS, payload: photos});
export const fetchPhotosFailure = err => ({type: FETCH_PHOTOS_FAILURE, payload: err});

export const fetchPhotos = (id) => {
    return async dispatch => {
        dispatch(fetchPhotosRequest());

        try {
            let response;
            if (id) {
                response = await axiosApi.get(`/photos/${id}`);
            } else {
                response = await axiosApi.get("/photos");
            }
            dispatch(fetchPhotosSuccess(response.data));
        } catch (e) {
            dispatch(fetchPhotosFailure(e));
        }
    };
};


export const CREATE_PHOTO_REQUEST = "CREATE_PHOTO_REQUEST";
export const CREATE_PHOTO_SUCCESS = "CREATE_PHOTO_SUCCESS";
export const CREATE_PHOTO_FAILURE = "CREATE_PHOTO_FAILURE";

export const createPhotoRequest = () => ({type: CREATE_PHOTO_REQUEST});
export const createPhotoSuccess = photo => ({type: CREATE_PHOTO_SUCCESS, payload: photo});
export const createPhotoFailure = err => ({type: CREATE_PHOTO_FAILURE, payload: err});

export const createPhoto = (photoData) => {
    return async (dispatch, getState) => {
        dispatch(createPhotoRequest());

        try {
            await axiosApi.post(
                "/photos",
                photoData,
                {headers: {
                        "Authorization": getState().users.user.token,
                    }}
            );
            dispatch(createPhotoSuccess());
            dispatch(historyPush(`/`));
        } catch (e) {
            if (e.response.data) {
                dispatch(createPhotoFailure(e.response.data));
            }
        }
    };
};


export const DELETE_PHOTO_REQUEST = "DELETE_PHOTO_REQUEST";
export const DELETE_PHOTO_SUCCESS = "DELETE_PHOTO_SUCCESS";
export const DELETE_PHOTO_FAILURE = "DELETE_PHOTO_FAILURE";

export const deletePhotoRequest = () => ({type: DELETE_PHOTO_REQUEST});
export const deletePhotoSuccess = photo => ({type: DELETE_PHOTO_SUCCESS, payload: photo});
export const deletePhotoFailure = err => ({type: DELETE_PHOTO_FAILURE, payload: err});

export const deletePhoto = (id) => {
    return async (dispatch, getState) => {
        dispatch(deletePhotoRequest());

        try {
            const response = await axiosApi.delete(
                `/photos/${id}`,
                {headers: {
                        "Authorization": getState().users.user.token,
                    }}
            );
            dispatch(deletePhotoSuccess(response.data));
        } catch (e) {
            dispatch(deletePhotoFailure(e));
        }
    };
};