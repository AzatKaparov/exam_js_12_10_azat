import {
    CREATE_PHOTO_FAILURE,
    CREATE_PHOTO_REQUEST, CREATE_PHOTO_SUCCESS,
    DELETE_PHOTO_FAILURE,
    DELETE_PHOTO_REQUEST, DELETE_PHOTO_SUCCESS,
    FETCH_PHOTOS_FAILURE,
    FETCH_PHOTOS_REQUEST,
    FETCH_PHOTOS_SUCCESS
} from "../actions/photosActions";

const initialState = {
    photosLoading: false,
    photosError: null,
    photos: [],
};

const photosReducer = (state=initialState, action) => {
    switch (action.type) {
        case FETCH_PHOTOS_REQUEST:
            return {...state, photosLoading: true};
        case FETCH_PHOTOS_SUCCESS:
            return {...state, photos: action.payload, photosLoading: false, photosError: null};
        case FETCH_PHOTOS_FAILURE:
            return {...state, photos: [], photosLoading: false, photosError: action.payload};
        case DELETE_PHOTO_REQUEST:
            return {...state, photosLoading: true};
        case DELETE_PHOTO_SUCCESS:
            const filtered = state.photos.filter(item => item._id !== action.payload._id);
            return {...state, photosLoading: false, photos: filtered};
        case DELETE_PHOTO_FAILURE:
            return {...state, photosLoading: false, photosError: action.payload};
        case CREATE_PHOTO_REQUEST:
            return {...state, photosLoading: true};
        case CREATE_PHOTO_SUCCESS:
            return {...state, photosLoading: false, photosError: null};
        case CREATE_PHOTO_FAILURE:
            return {...state, photosLoading: false, photosError: action.payload};
        default:
            return state;
    }
};

export default  photosReducer;