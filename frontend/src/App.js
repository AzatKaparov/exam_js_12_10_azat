import {Redirect, Route, Switch} from "react-router-dom";
import Layout from "./components/UI/Layout/Layout";
import Gallery from "./containers/Gallery/Gallery";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import UserGallery from "./containers/UserGallery/UserGallery";
import AddPhoto from "./containers/AddPhoto/AddPhoto";
import {useSelector} from "react-redux";


const App = () => {
    const user = useSelector(state => state.users.user);

    const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
        return isAllowed ?
            <Route {...props}/> :
            <Redirect to={redirectTo}/>
    };

  return (
      <div className="App">
        <Layout>
          <Switch>
            <Route path="/" exact component={Gallery}/>
            <Route path="/gallery/:id" exact component={UserGallery}/>
            <Route path="/register" exact component={Register}/>
            <Route path="/login" exact component={Login}/>
            <ProtectedRoute
                  path="/add-photo"
                  component={AddPhoto}
                  isAllowed={!!user}
                  redirectTo="/login"
                  exact
              />
            <Route render={() => <h1>Not found</h1>}/>
          </Switch>
        </Layout>
      </div>
  );
};

export default App;
